using System.IO;
using Microsoft.EntityFrameworkCore;

namespace Task1.Models
{
    public class DBContext: DbContext
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }
        public DbSet<FileModel> Files { get; set; }
    }
}