using System;

namespace Task1.Models
{
    public class FileModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public byte[] File { get; set; }
        public int DownloadsCounter { get; set; }
        public DateTime When { get; set; } = DateTime.Now;
    }
}