﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task1.Models;

namespace Task1.Controllers
{
    public class HomeController : Controller
    {
        DBContext _context;
        
        public HomeController(DBContext context)
        {
            _context = context;
        }        
        
        public IActionResult Index()
        {
            var res = _context.Files.ToList();
            return View(res);
        }

        public IActionResult AddFile() => View();

        public IActionResult Sort(string sorttype)
        {
            var res = _context.Files.ToList();
            //var sorttype = Request.Form["sorttype"].ToString();
            switch (sorttype)
            {
                case "date":
                    res=_context.Files.OrderBy(x=>x.When).ToList();
                    break;
                case "name":
                    res = _context.Files.OrderBy(x => x.Name).ToList();
                    break;
            }

            return View(res);
        }

        [HttpPost]
        public async Task<IActionResult> ConfigureFile(FileModel f,IFormFile uploadedFile)
        {
            if (!Directory.Exists("files/")) {
                Directory.CreateDirectory("files/");
            }
            if (uploadedFile != null)
            {
                string path = "files/" + uploadedFile.FileName;
                if (System.IO.File.Exists(path))
                    path=path.Insert(path.IndexOf('.'),Directory
                        .GetFileSystemEntries("files/")
                        .Count(x => x.StartsWith(uploadedFile.FileName.Split('.')[0])).ToString());
                using (var fileStream = new FileStream(path, FileMode.Create))
                    await uploadedFile.CopyToAsync(fileStream);
                
                var file = new FileModel()
                {
                    Name = f.Name,
                    Description = f.Description,
                    Path = path, 
                    
                };
                using (var memoryStream = new MemoryStream())
                {
                    await uploadedFile.CopyToAsync(memoryStream);              
                    file.File = memoryStream.ToArray();
                }
                _context.Files.Add(file);
                _context.SaveChanges();
            }
            
            return RedirectToAction("Index");
        }
        
        public IActionResult Download()
        {
            var bookId = int.Parse(Request.Query["fileId"]);
            var f = _context.Files.Find(bookId);
            f.DownloadsCounter++;
            _context.SaveChanges();
            FileInfo file = new FileInfo(f.Path);
            if (file.Exists)
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(file.FullName);
                var contentType = "application/octet-stream";
                return File(fileBytes,contentType, file.Name);
            }
            return View();
        }

        public IActionResult FilePage()
        {
            var bookId = int.Parse(Request.Query["fileId"]);
            var f = _context.Files.Find(bookId);
            FileInfo file = new FileInfo(f.Path);
            return View(f);
        }

        public async Task<IActionResult> Search()
        {
            var files = from b in _context.Files select b;
            var searchResult = Request.Query["search"];
            if (!string.IsNullOrEmpty(searchResult))
                files = _context.Files.Where(file => file.Name.Contains(searchResult));
            return View(await files.ToListAsync());
        }
    }
}