﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Task2.Data;
using Task2.Models;

namespace Task2.Controllers
{
    public class HomeController : Controller
    {
        private readonly FoodContext context;
        private readonly ApplicationDbContext usercontext;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly IHostingEnvironment appEnvironment;

        public HomeController(
            FoodContext context, ApplicationDbContext usercontext,
            RoleManager<IdentityRole> roleManager,
            UserManager<IdentityUser> userManager,
            IHostingEnvironment appEnvironment)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.usercontext = usercontext;
            this.context = context;
            this.appEnvironment = appEnvironment;
        }
        public IActionResult Index()
        {
            return View(context.Restaurants);
        }

        public IActionResult Rest(int restId)
        {
            return View(context.Dishes.Where(x => x.RestaurantId == restId));
        }

        public async Task<IActionResult> AddDish(int dishId)
        {
            var u = await userManager.GetUserAsync(User);
            var o = context.Orders.First(x => x.UserId == u.Id) ?? new Order()
            {
                UserId = u.Id
            };
            context.Orders.Update(o);
            context.DishOrders.Add(new DishOrder()
            {
                DishId = dishId,
                OrderId = o.OrderId
            });
            return View();
        }
    }
}