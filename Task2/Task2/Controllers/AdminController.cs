using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Task2.Data;
using Task2.Models;

namespace Task2.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController: Controller
    {
        private readonly FoodContext context;
        private readonly ApplicationDbContext usercontext;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly IHostingEnvironment appEnvironment;

        public AdminController(
            FoodContext context, ApplicationDbContext usercontext,
            RoleManager<IdentityRole> roleManager,
            UserManager<IdentityUser> userManager,
            IHostingEnvironment appEnvironment)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.usercontext = usercontext;
            this.context = context;
            this.appEnvironment = appEnvironment;
        }
        
        public IActionResult Index()
        {
            return View(context.Restaurants);
        }

        public IActionResult AddRestaurant() => View();

        public IActionResult AddDish(int restId)
        {
            var d = new Dish(){RestaurantId = restId};
            context.Dishes.Add(d);
            return View(d);
        }
        public IActionResult AddCode() => View();
        public IActionResult Food() => View(context.Dishes);
        public IActionResult Codes() => View(context.Promocodes);

        [HttpPost]
        public async Task<ActionResult> Create(Dish dish)
        {
            context.Dishes.Add(dish);
            context.SaveChanges();
            return View(context.Restaurants);
            Console.WriteLine(dish.RestaurantId);
            return RedirectToAction("Index");
        }
        public IActionResult ChooseRest(int restId)
        {
            var d = context.Dishes.Last();
            d.RestaurantId = restId;
            context.Dishes.Update(d);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<ActionResult> CreateCode(Promocode code)
        {
            context.Promocodes.Add(code);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        
        public IActionResult ConfigureRest(IFormCollection form)
        {
            var r = new Restaurant()
            {
                Name = form["Name"].ToString()
            };
            context.Restaurants.Add(r);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        
        public ActionResult Delete(int restId, int dishId)
        {
            Restaurant deletedRest = context.Restaurants.FirstOrDefault(x => x.Id == restId);
            if (deletedRest != null)
            {
                context.Restaurants.Remove(deletedRest);
                context.SaveChanges();
            }
            Dish deletedDish = context.Dishes.FirstOrDefault(x => x.Id == dishId);
            if (deletedDish != null)
            {
                context.Dishes.Remove(deletedDish);
                context.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}