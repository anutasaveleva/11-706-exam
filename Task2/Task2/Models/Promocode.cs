using System;

namespace Task2.Models
{
    public class Promocode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int Discount { get; set; }
        public DateTime ValidUntil { get; set; } = DateTime.Now.AddDays(1);
        public int UseCounter { get; set; } = 1;
    }
}