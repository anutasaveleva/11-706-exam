using System;

namespace Task2.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public string UserId { get; set; }
        public float TotalSum { get; set; }
        public DateTime When { get; set; } = DateTime.Now;
    }
}