namespace Task2.Models
{
    public class Dish
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public byte[] Image { get; set; }
        //Сделано так потому что у каждого заведения своя цена и предлагаются различные вариации
        public int RestaurantId { get; set; }
    }
}