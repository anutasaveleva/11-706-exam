namespace Task2.Models
{
    public class DishOrder
    {
        public int DishId { get; set; }
        public int OrderId { get; set; }
    }
}