using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Task2.Data
{
    public class RoleInitializer
    {
        public static async Task InitializeAsync(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
                {
                    string adminEmail = "admin@mail.com";
                    string password = "Aa123456!";
        
                    if (await roleManager.FindByNameAsync("admin") == null)
                        await roleManager.CreateAsync(new IdentityRole("admin"));
        
                    if (await userManager.FindByEmailAsync(adminEmail) == null)
                    {
                        IdentityUser admin = new IdentityUser { Email = adminEmail, UserName = adminEmail };
                        IdentityResult result = await userManager.CreateAsync(admin, password);
                        if (result.Succeeded)
                        {
                            await userManager.AddToRoleAsync(admin, "admin");
                        }
                    }
                }
    }
}