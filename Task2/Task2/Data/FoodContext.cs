using Microsoft.EntityFrameworkCore;
using Task2.Models;

namespace Task2.Data
{
    public class FoodContext: DbContext
    {
        public FoodContext(DbContextOptions<FoodContext> options)
            : base(options)
        {
        }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Promocode> Promocodes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<DishOrder> DishOrders { get; set; }
    }
}